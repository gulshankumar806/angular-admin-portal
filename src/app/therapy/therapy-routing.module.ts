import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Page404Component } from '../authentication/page404/page404.component';
import { ScheduleAppointmentComponent } from './schedule-appointment/schedule-appointment.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BookAppointmentComponent } from './book-appointment/book-appointment.component';
import { PatientsListComponent } from './patients-list/patients-list.component';
import { PatientDetailComponent } from './patient-detail/patient-detail.component';
import { AddEvaluationComponent } from './add-evaluation/add-evaluation.component';

const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'schedule-appointment',
    component: ScheduleAppointmentComponent
  },
  {
    path: 'book-appointment',
    component: BookAppointmentComponent
  },
  {
    path: 'patients-list',
    component: PatientsListComponent
  },
  {
    path: 'patient-detail',
    component: PatientDetailComponent
  },
  {
    path: 'add-evaluation',
    component: AddEvaluationComponent
  },
  { path: '**', component: Page404Component },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TherapyRoutingModule { }
