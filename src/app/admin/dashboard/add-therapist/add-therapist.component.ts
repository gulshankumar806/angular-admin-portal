import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-therapist',
  templateUrl: './add-therapist.component.html',
  styleUrls: ['./add-therapist.component.sass']
})
export class AddTherapistComponent implements OnInit {

  addTherapyForm: FormGroup;
  
  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {    
      this.addTherapyForm = this.fb.group({
        Name: ['', [Validators.required]],
        PhoneNo: ['', [Validators.required, Validators.pattern(/\d/)]],
        SpecialisedIn: ['', Validators.required],
        EmailId: ['', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]]
      });
  }

}
