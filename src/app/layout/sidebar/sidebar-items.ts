import { RouteInfo } from './sidebar.metadata';
export const ROUTES: RouteInfo[] = [
  
  // Admin Modules

  {
    path: '/admin/dashboard/main',
    title: 'Dashboard',
    moduleName: 'dashboard',
    iconType: 'material-icons',
    icon: 'dashboard',
    class: '',
    groupTitle: false,
    badge: '',
    badgeClass: '',
    role: ['Admin'],
    submenu: [],
  },
  {
    path: '/admin/dashboard/scheduled-appointments',
    title: 'Scheduled Appointments',
    moduleName: 'scheduled_appointments',
    iconType: 'material-icons',
    icon: 'event_note',
    class: '',
    groupTitle: false,
    badge: '',
    badgeClass: '',
    role: ['Admin'],
    submenu: [],
  },
  {
    path: '/admin/dashboard/therapists-list',
    title: 'Therapists List',
    moduleName: 'therapists_list',
    iconType: 'material-icons',
    icon: 'addchart',
    class: '',
    groupTitle: false,
    badge: '',
    badgeClass: '',
    role: ['Admin'],
    submenu: [],
  },
  {
    path: '/admin/dashboard/patients-list',
    title: 'Patients List',
    moduleName: 'patients_ist',
    iconType: 'material-icons',
    icon: 'perm_contact_calendar',
    class: '',
    groupTitle: false,
    badge: '',
    badgeClass: '',
    role: ['Admin'],
    submenu: [],
  },
  {
    path: 'javascript:void(0)',
    title: 'Chat',
    moduleName: 'chat',
    iconType: 'material-icons',
    icon: 'question_answer',
    class: '',
    groupTitle: false,
    badge: '',
    badgeClass: '',
    role: ['Admin'],
    submenu: [],
  },

  // Patient Modules
  
  {
    path: '/patient/dashboard',
    title: 'Dashboard',
    moduleName: 'dashboard',
    iconType: 'material-icons',
    icon: 'dashboard',
    class: '',
    groupTitle: false,
    badge: '',
    badgeClass: '',
    role: ['Patient'],
    submenu: [],
  },  
  {
    path: '/patient/patient-detail',
    title: 'Profile',
    moduleName: 'Profile',
    iconType: 'material-icons',
    icon: 'person',
    class: '',
    groupTitle: false,
    badge: '',
    badgeClass: '',
    role: ['Patient'],
    submenu: [],
  },  
  {
    path: '/patient/schedule-appointment',
    title: 'Schedule Appointment',
    moduleName: 'schedule_appointment',
    iconType: 'material-icons',
    icon: 'event_note',
    class: '',
    groupTitle: false,
    badge: '',
    badgeClass: '',
    role: ['Patient'],
    submenu: [],
  }, 
  {
    path: 'javascript:void(0)',
    title: 'Chat',
    moduleName: 'chat',
    iconType: 'material-icons',
    icon: 'question_answer',
    class: '',
    groupTitle: false,
    badge: '',
    badgeClass: '',
    role: ['Patient'],
    submenu: [],
  },

  // Therapy Modules
  
  {
    path: '/therapy/dashboard',
    title: 'Dashboard',
    moduleName: 'dashboard',
    iconType: 'material-icons',
    icon: 'dashboard',
    class: '',
    groupTitle: false,
    badge: '',
    badgeClass: '',
    role: ['Therapy'],
    submenu: [],
  },  
  {
    path: '/therapy/schedule-appointment',
    title: 'Schedule Appointment',
    moduleName: 'schedule_appointment',
    iconType: 'material-icons',
    icon: 'event_note',
    class: '',
    groupTitle: false,
    badge: '',
    badgeClass: '',
    role: ['Therapy'],
    submenu: [],
  },
  {
    path: '/therapy/patients-list',
    title: 'Patients List',
    moduleName: 'patients_ist',
    iconType: 'material-icons',
    icon: 'perm_contact_calendar',
    class: '',
    groupTitle: false,
    badge: '',
    badgeClass: '',
    role: ['Therapy'],
    submenu: [],
  },
  {
    path: 'javascript:void(0)',
    title: 'Chat',
    moduleName: 'chat',
    iconType: 'material-icons',
    icon: 'question_answer',
    class: '',
    groupTitle: false,
    badge: '',
    badgeClass: '',
    role: ['Therapy'],
    submenu: [],
  },
];
