import { DashboardComponent as studentDashboard } from './../../student/dashboard/dashboard.component';
import { DashboardComponent as teacherDashboard } from './../../teacher/dashboard/dashboard.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { Dashboard2Component } from './dashboard2/dashboard2.component';
import { ScheduledAppointmentsComponent } from './scheduled-appointments/scheduled-appointments.component';
import { TherapistsListComponent } from './therapists-list/therapists-list.component';
import { PatientsListComponent } from './patients-list/patients-list.component';
import { AddTherapistComponent } from './add-therapist/add-therapist.component';
import { TherapistDetailComponent } from './therapist-detail/therapist-detail.component';
import { PatientDetailComponent } from './patient-detail/patient-detail.component';
import { AddPatientComponent } from './add-patient/add-patient.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'main',
    pathMatch: 'full',
  },
  {
    path: 'main',
    component: MainComponent,
  },
  {
    path: 'dashboard2',
    component: Dashboard2Component,
  },
  {
    path: 'teacher-dashboard',
    component: teacherDashboard,
  },
  {
    path: 'student-dashboard',
    component: studentDashboard,
  },
  {
    path: 'scheduled-appointments',
    component: ScheduledAppointmentsComponent,
  },
  {
    path: 'therapists-list',
    component: TherapistsListComponent,
  },
  {
    path: 'patients-list',
    component: PatientsListComponent,
  },
  {
    path: 'add-therapist',
    component: AddTherapistComponent,
  },
  {
    path: 'therapist-detail',
    component: TherapistDetailComponent,
  },
  {
    path: 'patient-detail',
    component: PatientDetailComponent,
  },
  {
    path: 'add-patient',
    component: AddPatientComponent,
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule {}
