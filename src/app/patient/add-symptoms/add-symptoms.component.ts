import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-add-symptoms',
  templateUrl: './add-symptoms.component.html',
  styleUrls: ['./add-symptoms.component.sass']
})
export class AddSymptomsComponent implements OnInit {

  addSymptomsForm: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {  
    this.addSymptomsForm = this.fb.group({
      Title: ['', [Validators.required]],
      Strength: ['', Validators.required],
      Date: ['', Validators.required],
      Time: ['', Validators.required],
      Duration: ['', Validators.required]
    });
  }

}
