import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-diet',
  templateUrl: './add-diet.component.html',
  styleUrls: ['./add-diet.component.sass']
})
export class AddDietComponent implements OnInit {

  addDietForm: FormGroup;
  selectedFile = null;

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {  
    this.addDietForm = this.fb.group({
      Title: ['', [Validators.required]],
      Date: ['', Validators.required],
      Time: ['', Validators.required],
      Quantity: ['', Validators.required],
      Unit:  ['', Validators.required]
    });
  }

  onFileSelected(event)
  {
    this.selectedFile = event.target.files[0];
  }

}
