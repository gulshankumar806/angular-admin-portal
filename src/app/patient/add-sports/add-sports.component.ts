import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-sports',
  templateUrl: './add-sports.component.html',
  styleUrls: ['./add-sports.component.sass']
})
export class AddSportsComponent implements OnInit {

  addSportForm: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {  
    this.addSportForm = this.fb.group({
      Title: ['', [Validators.required]],
      Date: ['', Validators.required],
      Time: ['', Validators.required],
      Duration: ['', Validators.required]
    });
  }

}
