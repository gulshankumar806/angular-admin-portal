import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { MainComponent } from './main/main.component';
import { Dashboard2Component } from './dashboard2/dashboard2.component';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgApexchartsModule } from 'ng-apexcharts';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { ScheduledAppointmentsComponent } from './scheduled-appointments/scheduled-appointments.component';
import { TherapistsListComponent } from './therapists-list/therapists-list.component';
import { PatientsListComponent } from './patients-list/patients-list.component';
import { AddTherapistComponent } from './add-therapist/add-therapist.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { TherapistDetailComponent } from './therapist-detail/therapist-detail.component';
import { PatientDetailComponent } from './patient-detail/patient-detail.component';
import { AddPatientComponent } from './add-patient/add-patient.component';


import {MatTabsModule} from '@angular/material/tabs';

import {
  MatStepperModule
} from '@angular/material/stepper';

@NgModule({
  declarations: [MainComponent, Dashboard2Component, ScheduledAppointmentsComponent, TherapistsListComponent, PatientsListComponent, AddTherapistComponent, TherapistDetailComponent, PatientDetailComponent, AddPatientComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts'),
    }),
    PerfectScrollbarModule,
    MatIconModule,
    NgApexchartsModule,
    MatButtonModule,
    MatMenuModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatStepperModule,
    MatTabsModule
  ],
})
export class DashboardModule {}
