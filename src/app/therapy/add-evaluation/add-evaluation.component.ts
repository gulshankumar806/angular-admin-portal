import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-evaluation',
  templateUrl: './add-evaluation.component.html',
  styleUrls: ['./add-evaluation.component.sass']
})
export class AddEvaluationComponent implements OnInit {

  addEvaluateForm: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {  
    this.addEvaluateForm = this.fb.group({
      // patientName: ['', [Validators.required]],
    });
  }

}
