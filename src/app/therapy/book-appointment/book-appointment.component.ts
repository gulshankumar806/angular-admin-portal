import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-book-appointment',
  templateUrl: './book-appointment.component.html',
  styleUrls: ['./book-appointment.component.sass']
})
export class BookAppointmentComponent implements OnInit {

  bookApptForm: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {  
    this.bookApptForm = this.fb.group({
      patientName: ['', [Validators.required]],
      Date: ['', Validators.required],
      Time: ['', Validators.required],
      Allowed: ['', Validators.required],
      Scheduled: ['', Validators.required],
    });
  }

}
