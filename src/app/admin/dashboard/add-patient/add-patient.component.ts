import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';;
import { MAT_STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';

@Component({
  selector: 'app-add-patient',
  templateUrl: './add-patient.component.html',
  styleUrls: ['./add-patient.component.sass'],
  providers: [{
    provide: MAT_STEPPER_GLOBAL_OPTIONS, useValue: { displayDefaultIndicatorType: false }
  }]
})
export class AddPatientComponent implements OnInit {

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.firstFormGroup = this.fb.group({
      Name: [''],
      PhoneNo: [''],
      Email: [''],
      DOB: [''],
    });
    this.secondFormGroup = this.fb.group({
      Address: [''],
      State: [''],
      Country: [''],
      PostalCode: [''],
    });
    this.thirdFormGroup = this.fb.group({
      Occupation: [''],
      ScopeOfEmploy: [''],
      ShiftWork: [''],
    });
    this.fourthFormGroup = this.fb.group({
      Illness: [''],
      Height: [''],
      CurrentWeight: [''],
      DesiredWeight: [''],
      HealthInsuranceName: [''],
      HealthInsuranceNo: [''],
      Comment: [''],
      AllowedAppointments: [''],
      AvailedAppointments: [''],
      Diet: [''],
    });
  }

}
