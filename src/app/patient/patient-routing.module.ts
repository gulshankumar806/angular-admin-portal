import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PatientDetailComponent } from "../patient/patient-detail/patient-detail.component";
import { ScheduleAppointmentComponent } from '../patient/schedule-appointment/schedule-appointment.component';
import { Page404Component } from '../authentication/page404/page404.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AddSymptomsComponent } from './add-symptoms/add-symptoms.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { AddSportsComponent } from './add-sports/add-sports.component';
import { AddWeightComponent } from './add-weight/add-weight.component';
import { AddDietComponent } from './add-diet/add-diet.component';



const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent,
  },
  {
    path: 'patient-detail',
    component: PatientDetailComponent,
  },
  {
    path: 'schedule-appointment',
    component: ScheduleAppointmentComponent,
  },
  {
    path: 'add-symptoms',
    component: AddSymptomsComponent,
  },
  {
    path: 'edit-profile',
    component: EditProfileComponent,
  },
  {
    path: 'add-sports',
    component: AddSportsComponent,
  },
  {
    path: 'add-weight',
    component: AddWeightComponent,
  },
  {
    path: 'add-diet',
    component: AddDietComponent,
  },
  { path: '**', component: Page404Component },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientRoutingModule { }
