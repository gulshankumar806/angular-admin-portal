import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-weight',
  templateUrl: './add-weight.component.html',
  styleUrls: ['./add-weight.component.sass']
})
export class AddWeightComponent implements OnInit {

  addWeightForm: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {  
    this.addWeightForm = this.fb.group({
      Title: ['', [Validators.required]],
      Date: ['', Validators.required],
      Time: ['', Validators.required]
    });
  }

}
